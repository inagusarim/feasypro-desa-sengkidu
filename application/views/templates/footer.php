    </main>
    <footer class="revealed">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3>Hubungi Kami</h3>
                    <a href="<?php echo $telephone_link ?>" id="phone"><?php echo $telephone ?></a>
                    <a href="<?php echo $email_link ?>" id="email_footer"><?php echo $email ?></a>
                    <div id="social_footer_2">
                        <ul>
                            <?php if ( $facebook_link != null)  { ?>
                                <li><a title="<?php echo $facebook_title ?>" href="<?php echo $facebook_link ?>"><i style="font-size: 25px;" class="icon-facebook"></i></a></li>
                            <?php } ?>
                            <?php if ( $instagram_link != null)  { ?>
                                <li><a title="<?php echo $instagram_title ?>" href="<?php echo $instagram_link ?>"><i style="font-size: 25px;" class="icon-instagram"></i></a></li>
                            <?php } ?>
                            <?php if ( $youtube_link != null)  { ?>
                                <li><a title="<?php echo $youtube_title ?>" href="<?php echo $youtube_link ?>"><i style="font-size: 25px;" class="icon-youtube-play"></i></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <h3>Lainnya</h3>
                    <ul>
                        <li style="padding-bottom: 10px;padding-left: 20px"><a href="#">Kebijakan Privasi</a></li>
                        <li style="padding-bottom: 10px;padding-left: 20px"><a href="#">Kontak Kami</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>Statistik Pengunjung</h3>
                    <table>
                        <tr>
                            <td style="padding-bottom: 10px;padding-left: 20px;">Pengunjung Hari ini</td>
                            <td style="padding-bottom: 10px;padding-left: 20px;">&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                            <td style="padding-bottom: 10px;padding-left: 20px;"><?php echo $visitor['pengunjunghariini'] ?> orang</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;padding-left: 20px;">Total Pengunjung</td>
                            <td style="padding-bottom: 10px;padding-left: 20px;">&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                            <td style="padding-bottom: 10px;padding-left: 20px;"><?php echo $visitor['totalpengunjung'] ?> orang</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;padding-left: 20px;">Pengunjung Online <i class="icon-dot-circled" style="color: green"></i></td>
                            <td style="padding-bottom: 10px;padding-left: 20px;">&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                            <td style="padding-bottom: 10px;padding-left: 20px;"><?php echo $visitor['pengunjungonline'] ?> orang</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-3">
                    <div style="height: 200px">
					    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3945.9983279245425!2d115.5459724143344!3d-8.499541688380793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd20f23c71b7f1f%3A0x5e499ba6877e3b6e!2sJl.%20Raya%20Sengkidu%2C%20Sengkidu%2C%20Kec.%20Manggis%2C%20Kabupaten%20Karangasem%2C%20Bali%2080871!5e0!3m2!1sen!2sid!4v1595497677191!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="social_footer">
                        <p>Copyright &copy; 2020 - <?php echo date ('Y');?> <a href="http://desa-sengkidu.desa.id" target="_blank" class="color-white">Desa Sengkidu</a> |  Powered by <a href="http://haihai.com" target="_blank" class="color-white">haihai</a> | All rights reserved </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

	<div id="toTop"></div>

	<div class="search-overlay-menu">
		<span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
		<form role="search" id="searchform" method="get">
			<input value="" name="q" type="search" placeholder="Search..." />
			<button type="submit"><i class="icon_set_1_icon-78"></i>
			</button>
		</form>
	</div>

	<script src="<?php echo base_url();?>assets/js/jquery-2.2.4.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/common_scripts_min.js"></script>
    <script src="<?php echo base_url();?>assets/js/functions.js"></script>

    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>

    <script>
        $(document).ready(function() {
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:2
                    },
                    1000:{
                        items:4
                    }
                }

            });
        });
    </script>
   

</body>

</html>