        <div id="carousel-home">
            <div class="owl-carousel owl-theme ">
                <?php foreach($sliders as $data_slider) { ?>
                     <div class="owl-slide cover" style="background-image: url(<?php echo base_url();?>upload/images/<?php echo $data_slider->thumbnail ?>);">
                        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.5)">
                            <div class="container">
                                <div class="row justify-content-center justify-content-md-start">
                                    <div class="col-lg-12 static">
                                        <div class="slide-text <?php echo $data_slider->position_text ?> white">
                                            <h2 class="owl-slide-animated owl-slide-title"><?php echo $data_slider->title ?></h2>
                                            <p class="owl-slide-animated owl-slide-subtitle">
                                                <?php echo $data_slider->description ?>
                                            </p>
                                            <?php if ($data_slider->url != null) { ?>
                                                <div class="owl-slide-animated owl-slide-cta"><a class="btn_1" href="<?php echo $data_slider->url ?>" role="button">Cari Tau Disini</a></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="icon_drag_mobile"></div>
        </div>

        <div class="white_bg">
			<div class="container margin_60">
				<div class="banner_2" style="background: url(<?php echo base_url();?>upload/images/<?php echo $banner->thumbnail ?>) center center no-repeat;background-size: cover">
                    <?php if ($banner->url) { ?>
                        <a href="<?php echo $banner->url ?>" class="btn_home_align btn_1 rounded">Cari Tau Disini</a>
                    <?php } ?>
                </div>
            </div>
		</div>

        <div class="container margin_30">
            <div class="main_title" >
				<h2 style="text-transform: unset; !important;"><span>Pengumuman</span> Terbaru Kami</h2>
			</div>
            <p class="btn_home_align"><a href="<?php echo site_url();?>pengumuman" class="btn_1 rounded">Lihat Lebih Banyak Pengumuman</a></p>
			<div class="row">
                <?php $n=1; foreach($pengumumans as $data_pengumuman) { ?>
                    <div class="col-lg-6 wow fadeIn" data-wow-delay="0.<?php echo $n ?>s">
                        <div class="feature">
                            <i class="pe-7s-speaker"></i>
                            <h3><span><?php echo $data_pengumuman->title ?></span></h3>
                            <p>
                                <?php echo $data_pengumuman->description ?>
                            </p>
                        </div>
                    </div>
                    <?php $n++ ?>
                <?php } ?>
            </div>
        </div>

        <div class="container margin_30">
            <div class="main_title">
				<h2 style="text-transform: unset; !important;"><span>Agenda</span> Terbaru Kami</h2>
			</div>
            <p class="btn_home_align"><a href="#" class="btn_1 rounded">Lihat Lebih Banyak Agenda</a></p>
			<div class="row">
                <?php $n=0; foreach($agendas as $data_agenda) { ?>
                    <div class="col-lg-4 wow zoomIn" data-wow-delay="0.<?php $n=$n+2; echo $n ?>s">
                        <div class="feature_home">

                            <div style="text-align: center;
                                        color: #fff;
                                        background-color: #e04f67;
                                        font-size: 27px;
                                        display: inline-block;
                                        width: 100%;
                                        padding: 17px;
                                        line-height: 1;">
                                <?php
                                    $y =  substr ($data_agenda->date,0,4);
                                    $m =  substr ($data_agenda->date,5,2);
                                    $d =  substr ($data_agenda->date,8,2);
                                    $monnth = $this->main->get_bulan($m);
                                    $date = $d." ".$monnth." ".$y;
                                    echo $date;
                                 ?>
                            </div>
                            <div style="padding: 30px 30px 0 30px">
                                <h3><span><?php echo $data_agenda->title ?></span></h3>
                                <p>
                                    <?php echo $data_agenda->description ?>
                                </p>
                            </div>

                        </div>
                    </div>
                    <?php $n++ ?>
                <?php } ?>
            </div>
        </div>

        <div class="container margin_30">
            <div class="main_title">
                <h2 style="text-transform: unset; !important;"><span>Berita</span> Terbaru Kami</h2>
            </div>
            <p class="btn_home_align"><a href="#" class="btn_1 rounded">Lihat Lebih Banyak Berita</a></p>
            <div class="row">
                <div class="col-lg-6">
                    <a class="box_news" href="#">
                        <figure><img src="<?php echo base_url();?>assets/img/news_home_1.jpg" alt="">
                            <figcaption><strong>28</strong>Dec</figcaption>
                        </figure>
                        <ul>
                            <li>Mark Twain</li>
                            <li>20.11.2017</li>
                        </ul>
                        <h4>Pri oportere scribentur eu</h4>
                        <p>Cu eum alia elit, usu in eius appareat, deleniti sapientem honestatis eos ex. In ius esse ullum vidisse....</p>
                    </a>
                </div>
                <div class="col-lg-6">
                    <a class="box_news" href="#">
                        <figure><img src="<?php echo base_url();?>assets/img/news_home_2.jpg" alt="">
                            <figcaption><strong>28</strong>Dec</figcaption>
                        </figure>
                        <ul>
                            <li>Jhon Doe</li>
                            <li>20.11.2017</li>
                        </ul>
                        <h4>Duo eius postea suscipit ad</h4>
                        <p>Cu eum alia elit, usu in eius appareat, deleniti sapientem honestatis eos ex. In ius esse ullum vidisse....</p>
                    </a>
                </div>
                <div class="col-lg-6">
                    <a class="box_news" href="#">
                        <figure><img src="<?php echo base_url();?>assets/img/news_home_3.jpg" alt="">
                            <figcaption><strong>28</strong>Dec</figcaption>
                        </figure>
                        <ul>
                            <li>Luca Robinson</li>
                            <li>20.11.2017</li>
                        </ul>
                        <h4>Elitr mandamus cu has</h4>
                        <p>Cu eum alia elit, usu in eius appareat, deleniti sapientem honestatis eos ex. In ius esse ullum vidisse....</p>
                    </a>
                </div>
                <div class="col-lg-6">
                    <a class="box_news" href="#">
                        <figure><img src="<?php echo base_url();?>assets/img/news_home_4.jpg" alt="">
                            <figcaption><strong>28</strong>Dec</figcaption>
                        </figure>
                        <ul>
                            <li>Paula Rodrigez</li>
                            <li>20.11.2017</li>
                        </ul>
                        <h4>Id est adhuc ignota delenit</h4>
                        <p>Cu eum alia elit, usu in eius appareat, deleniti sapientem honestatis eos ex. In ius esse ullum vidisse....</p>
                    </a>
                </div>
            </div>
        </div>

        <div class="container margin_60">
            <div class="row">
                <div class="owl-carousel owl-theme" style="height:285px !important;">
                    <?php foreach($galeris as $data_galeri) { ?>
                        <div class="col-md-12">
                            <img style="object-fit: cover !important;" alt="<?php echo $data_galeri->thumbnail_alt ?>" title="<?php echo $data_galeri->title ?>" src="<?php echo base_url();?>upload/images/<?php echo $data_galeri->thumbnail ?>">
                        </div>
                    <?php } ?>
                </div>

			</div>
		</div>