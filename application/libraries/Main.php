<?php

Class Main
{

    private $ci;
    private $web_name = 'Desa Sengkidu';
    private $file_info = 'Images with resolution 800px x 600px and size 100KB';
    private $file_info_slider = 'Images with resolution 1920px x 900px and size 250KB';
    private $path_images = 'upload/images/';
    private $image_size_preview = 200;
    private $help_thumbnail_alt = 'Penting untuk SEO Gambar';
    private $help_meta = 'Penting untuk SEO Halaman Website';
    private $short_desc_char = 100;

    public function __construct()
    {
        error_reporting(0);
        $this->ci =& get_instance();
    }

    function short_desc($string)
    {
        return substr(strip_tags($string), 0, $this->short_desc_char) . ' ...';
    }

    function web_name()
    {
        return $this->web_name;
    }

    function date_view($date)
    {
        return date('d F Y', strtotime($date));
    }

    function help_thumbnail_alt()
    {
        return $this->help_thumbnail_alt;
    }

    function help_meta()
    {
        return $this->help_meta;
    }

    function file_info()
    {
        return $this->file_info;
    }

    function file_info_slider()
    {
        return $this->file_info_slider;
    }

    function path_images()
    {
        return $this->path_images;
    }

    function image_size_preview()
    {
        return $this->image_size_preview;
    }

    function image_preview_url($filename)
    {
        return base_url($this->path_images . $filename);
    }

    function delete_file($filename)
    {
        if ($filename) {
            if (file_exists(FCPATH . $this->path_images . $filename)) {
			unlink($this->path_images . $filename);
            }
        }
    }

    function data_main($menu_active=null)
    {
        $data = array(
            'web_name' => $this->web_name,
            'menu_list' => $this->menu_list(),
            'name' => $this->ci->session->userdata('name'),
        );

        $data['current_url'] = $menu_active ? $menu_active : current_url();

        return $data;
    }

    function data_front()
    {
        $data = array(
            'name_website' => $this->web_name,
            'favicon' => 'desa-sengkidu-favicon.png',
            'logo_header' => 'logo-Desa-Sengkidu.png',
            'logo_header_2' => 'logo-Desa-Sengkidu-2.png',
            'alamat' => 'Kecamatan Manggis, Kabupaten Karangasem, Bali',
            'telephone' => '(0363) 123456',
            'telephone_link' => 'tel:0363123456',
            'email_link' => 'mailto:desa-sengkidu@gmail.com',
            'email' => 'desa-sengkidu@gmail.com',
            'whatsapp' => '',
            'whatsapp_link' => '',
            'facebook_link' => 'https://www.facebook.com/',
            'facebook_title' => 'Desa Sengkidu',
            'instagram_link' => 'https://www.instagram.com/',
            'instagram_title' => 'Desa Sengkidu',
            'youtube_link' => '',
            'youtube_title' => '',
            'view_secret' => FALSE,
            'author' => 'https://desa-sengkidu.desa.id/'
        );

        return $data;
    }

    function check_admin()
    {
        if ($this->ci->session->userdata('status') !== 'login') {
            redirect('feasypro');
        }
    }

    function check_login()
    {
        if ($this->ci->session->userdata('status') == 'login') {
            redirect('feasypro/dashboard');
        }
    }

    function permalink($data)
    {

        $slug = '';
        foreach ($data as $r) {
            $slug .= $this->slug($r) . '/';
        }

        return site_url($slug);
    }

    function breadcrumb($data)
    {
        $breadcrumb = '<ul class="breadcrumb">';
        $count = count($data);
        $no = 1;
        foreach ($data as $url => $label) {
            $current = '';
            if ($no == $count) {
                $current = ' class="current"';
            }

            $breadcrumb .= '<li' . $current . '><a href="' . $url . '">' . $label . '</a></li>';
        }

        $breadcrumb .= '</ul>';


        return $breadcrumb;
    }

    function slug($text)
    {

        $find = array(' ', '/', '&', '\\', '\'', ',','(',')');
        $replace = array('-', '-', 'and', '-', '-', '-','','');

        $slug = str_replace($find, $replace, strtolower($text));

        return $slug;
    }

    function date_format_view($date)
    {
        return date('d M Y', strtotime($date));
    }

    function get_bulan($data){
        $bulan = $data;
        Switch ($bulan){
            case 1 : $bulan="Januari";
                Break;
            case 2 : $bulan="Februari";
                Break;
            case 3 : $bulan="Maret";
                Break;
            case 4 : $bulan="April";
                Break;
            case 5 : $bulan="Mei";
                Break;
            case 6 : $bulan="Juni";
                Break;
            case 7 : $bulan="Juli";
                Break;
            case 8 : $bulan="Agustus";
                Break;
            case 9 : $bulan="September";
                Break;
            case 10 : $bulan="Oktober";
                Break;
            case 11 : $bulan="November";
                Break;
            case 12 : $bulan="Desember";
                Break;
            }
        return $bulan;
     }

    function slug_back($slug)
    {
        $slug = trim($slug);
        if (empty($slug)) return '';
        $slug = str_replace('-', ' ', $slug);
        $slug = ucwords($slug);
        return $slug;
    }

    function upload_file_thumbnail($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['max_width'] = 80000;
        $config['max_height'] = 60000;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function upload_file_slider($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 250;
        $config['max_width'] = 1920;
        $config['max_height'] = 1080;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function captcha()
    {
        $this->ci->load->helper(array('captcha', 'string'));
        $this->ci->load->library('session');

        $vals = array(
            'img_path' => './upload/images/captcha/',
            'img_url' => base_url() . 'upload/images/captcha',
            'font_path'  => './assets/css/fonts/mvboli.ttf',
            'img_width' => '200',
            'img_height' => 35,
            'border' => 0,
            'expiration' => 7200,
            'word' => random_string('numeric', 5)
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $captcha = $cap['image'];

        // store the captcha word in a session
        //$cap['word'];
        $this->ci->session->set_userdata('captcha_mwz', $cap['word']);

        return $captcha;
    }

    function share_link($socmed_type, $title, $link) {
        switch($socmed_type) {
            case "facebook":
                return "https://www.facebook.com/sharer/sharer.php?u=".$link;
                break;
            case "twitter":
                return "https://twitter.com/home?status=".$link;
                break;
            case "googleplus":
                return "https://plus.google.com/share?url=".$link;
                break;
            case "linkedin":
                return "https://www.linkedin.com/shareArticle?mini=true&url=".$link."&title=".$title."&summary=&source=";
                break;
            case "pinterest":
                return "https://pinterest.com/pin/create/button/?url=".$title."&media=".$link."&description=";
                break;
            case "email":
                return "mailto:".$link."?&subject=".$title;
            default:
                return $link;
                break;
        }
    }

    function mailer_auth($subject, $to_email, $to_name, $body)
    {
        $this->ci->load->library('my_phpmailer');
        $mail = new PHPMailer;

        try {
            $mail->IsSMTP();
            $mail->SMTPSecure = "ssl";
            $mail->Host = "mail.redconsulting.co.id"; //hostname masing-masing provider email
            $mail->SMTPDebug = 2;
            $mail->SMTPDebug = FALSE;
            $mail->do_debug = 0;
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->Username = "support@redconsulting.co.id"; //user email
            $mail->Password = "Support123!@#"; //password email
            $mail->SetFrom("support@redconsulting.co.id", $this->web_name); //set email pengirim
            $mail->Subject = $subject; //subyek email
            $mail->AddAddress($to_email, $to_name); //tujuan email
            $mail->MsgHTML($body);
            $mail->Send();
            //echo "Message has been sent";
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    function visitor(){
        $ip    = $this->ci->input->ip_address();
        $date  = date("Y-m-d");
        $waktu = time(); //
        $timeinsert = date("Y-m-d H:i:s");

        $s = $this->ci->db->query("SELECT * FROM visitor WHERE ip='".$ip."' AND date='".$date."'")->num_rows();
        $ss = isset($s)?($s):0;

        if($ss == 0){
        $this->ci->db->query("INSERT INTO visitor(ip, date, hits, online, time) VALUES('".$ip."','".$date."','1','".$waktu."','".$timeinsert."')");
        } else {
        $this->ci->db->query("UPDATE visitor SET hits=hits+1, online='".$waktu."' WHERE ip='".$ip."' AND date='".$date."'");
        }

        $pengunjunghariini  = $this->ci->db->query("SELECT * FROM visitor WHERE date='".$date."' GROUP BY ip")->num_rows();
        $dbpengunjung = $this->ci->db->query("SELECT COUNT(hits) as hits FROM visitor")->row();
        $totalpengunjung = isset($dbpengunjung->hits)?($dbpengunjung->hits):0;
        $bataswaktu = time() - 300;
        $pengunjungonline  = $this->ci->db->query("SELECT * FROM visitor WHERE online > '".$bataswaktu."'")->num_rows();

        $data = array(
            'pengunjunghariini' => $pengunjunghariini,
            'totalpengunjung' => $totalpengunjung,
            'pengunjungonline' => $pengunjungonline
        );

        return $data;
    }

    function menu_list()
    {
        $menu = array(
            'MAIN' => array(
                'dashboard' => array(
                    'label' => 'Dashboard',
                    'route' => base_url('feasypro/dashboard'),
                    'icon' => 'flaticon-layers',
                    'sub_menu' => array()
                ),
                'view_front' => array(
                    'label' => 'View Website',
                    'route' => base_url(''),
                    'icon' => 'flaticon-responsive',
                    'sub_menu' => array()
                )
            ),
            'PAGES' => array(
                'home' => array(
                    'label' => 'Beranda',
                    'route' => 'javascript:;',
                    'icon' => 'flaticon-buildings',
                    'sub_menu' => array(
                        'home_page' => array(
                            'label' => 'Halama Umum Beranda',
                            'route' => base_url('feasypro/pages/type/home'),
                            'icon' => 'flaticon-buildings'
                        ),
                        'home_slider' => array(
							'label' => 'Slider',
							'route' => base_url('feasypro/slider'),
							'icon' => 'fa fa-asymmetrik'
						),
                        'home_banner' => array(
							'label' => 'Banner',
							'route' => base_url('feasypro/banner'),
							'icon' => 'fa fa-asymmetrik'
						),
                    )
                ),
                'about' => array(
                    'label' => 'Profil Desa',
                    'route' => 'javascript:;',
                    'icon' => 'flaticon-home',
                    'sub_menu' => array(
                        'about_page' => array(
                            'label' => 'Halama Umum Profil Desa',
                            'route' => base_url('feasypro/pages/type/about'),
                            'icon' => 'fab fa-asymmetrik'
                        ),


                    )
                ),
                'pengumumans' => array(
                    'label' => 'Pengumuman',
                    'route' => 'javascript:;',
                    'icon' => 'flaticon-exclamation-square',
                    'sub_menu' => array(
                        'pengumuman_page' => array(
                            'label' => 'Halama Umum Pengumuman',
                            'route' => base_url('feasypro/pages/type/pengumuman'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'pengumuman' => array(
                            'label' => 'Pengumuman',
                            'route' => base_url('feasypro/pengumuman'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'agendas' => array(
                    'label' => 'Agenda',
                    'route' => 'javascript:;',
                    'icon' => 'flaticon-event-calendar-symbol',
                    'sub_menu' => array(
                        'agenda_page' => array(
                            'label' => 'Halama Umum Agenda',
                            'route' => base_url('feasypro/pages/type/agenda'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'agenda' => array(
                            'label' => 'Agenda',
                            'route' => base_url('feasypro/agenda'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'beritas' => array(
                    'label' => 'Berita',
                    'route' => 'javascript:;',
                    'icon' => 'flaticon-book',
                    'sub_menu' => array(
                        'berita_page' => array(
                            'label' => 'Halama Umum Berita',
                            'route' => base_url('feasypro/pages/type/berita'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'berita_kategori' => array(
                            'label' => 'Kategori Berita',
                            'route' => base_url('feasypro/berita_kategori'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'berita' => array(
                            'label' => 'Berita',
                            'route' => base_url('feasypro/berita'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'galeris' => array(
                    'label' => 'Galeri',
                    'route' => 'javascript:;',
                    'icon' => 'flaticon-app',
                    'sub_menu' => array(
                        'galeri_page' => array(
                            'label' => 'Halama Umum Galeri',
                            'route' => base_url('feasypro/pages/type/galeri'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'galeri' => array(
                            'label' => 'Galeri',
                            'route' => base_url('feasypro/galeri'),
                            'icon' => 'fab fa-asymmetrik'
                        ),


                    )
                ),

            ),
            'OTHERS MENU' => array(
                'email' => array(
                    'label' => 'Email',
                    'route' => base_url('feasypro/email'),
                    'icon' => 'flaticon-email',
                    'sub_menu' => array()
                ),
                'admin' => array(
                    'label' => 'Manage Admin',
                    'route' => base_url('feasypro/admin'),
                    'icon' => 'flaticon-users',
                    'sub_menu' => array()
                ),
            ),
        );

        return $menu;
    }
}
