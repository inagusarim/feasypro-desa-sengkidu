<div class="container margin_60">
			<div class="main_title">
				<h2>Some <span>images</span> from travellers</h2>
				<p>
					Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.
				</p>
			</div>
			<hr>

			<div id="gallery" class="final-tiles-gallery effect-dezoom effect-fade-out caption-top social-icons-right">
				<div class="ftg-items">
                    <?php foreach($galeris as $data_galeri) { ?>
                        <div class="tile">
                            <a class="tile-inner" data-title="<?php echo $data_galeri->title ?>" data-lightbox="gallery" href="<?php echo base_url();?>upload/images/<?php echo $data_galeri->thumbnail ?>">
                                <img class="item" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="<?php echo base_url();?>upload/images/<?php echo $data_slider->thumbnail ?>" alt="Image">
                                <span class='title'><?php echo $data_galeri->title ?></span>
                                <span class='subtitle'><?php echo $data_galeri->discription ?></span>
                            </a>

                        </div>
					 <?php } ?>



				</div>
			</div>

		</div>