<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php echo $page->meta_title ?></title>
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>"/>
    <meta name="description" content="<?php echo $page->meta_description ?>">
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <meta content="telephone=no" name="format-detection">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta name="author" content="<?php echo $author ?>"/>
    <meta name="MSSmartTagsPreventParsing" content="true"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="reply-to" content="info@redsystem.id">
    <meta http-equiv="expires" content="Mon, 28 Jul <?php echo date('Y')+1;?> 11:12:01 GMT">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/<?php echo $favicon ?>" type="image/x-icon">

    <link href="<?php echo base_url();?>assets/css/font/Gochi-Hand-Montserrat.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/vendors.css" rel="stylesheet">
    <!--page home-->
    <link href="<?php echo base_url();?>assets/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/owl.theme.default.min.css" rel="stylesheet">
    <!--end-->


    <!--end-->
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">

</head>

<body>

    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>

    <div class="layer"></div>
    <header class="sticky_base">
        <div id="top_line">
            <div class="container">
                <div class="row">
                    <div class="col-6"><a class="link--white" href="https://goo.gl/maps/eW61GZGq1KTazr916"><i class="icon-location"></i> <strong><?php echo $alamat ?></strong></a></div>
                    <div class="col-6">
                        <ul id="top_links">
                            <li>
                                <?php if ( $facebook_link != null)  { ?>
                                    <a href="<?php echo $facebook_link ?>"><i class="icon-facebook"></i></a>
                                <?php } ?>
                                <?php if ( $instagram_link != null)  { ?>
                                    <a href="<?php echo $instagram_link ?>"><i class="icon-instagram"></i></a>
                                <?php } ?>
                                <?php if ( $youtube_link != null)  { ?>
                                    <a href="<?php echo $youtube_link ?>"><i class="icon-youtube-play"></i></a>
                                <?php } ?>
                            </li>
                            <li><a href="<?php echo $email_link ?>"><i class="icon-mail"></i> <?php echo $email ?></a></li>
                            <li><a href="<?php echo $telephone_link ?>"><i class="icon-phone"></i> <?php echo $telephone ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-3">

                    <div id="logo">
						<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/<?php echo $logo_header ?>" width="160" height="auto" alt="Desa Sengkidu" data-retina="true" class="logo_normal"></a>
						<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/<?php echo $logo_header_2 ?>" width="160" height="auto" alt="Desa Sengkidu" data-retina="true" class="logo_sticky"></a>
					</div>
                </div>
                <nav class="col-9">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                    <div class="main-menu">
                        <div id="header_menu">
                            <img src="<?php echo base_url();?>assets/img/<?php echo $logo_header ?>" width="160" height="34" alt="City tours" data-retina="true">
                        </div>
                        <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                         <ul>
                            <li><a class="<?php if($this->session->menu=='home'){echo('current-menu');} ?> " href="<?php echo site_url();?>">Beranda</a></li>
                            <li class="submenu">
                                <a href="javascript:void(0);" class="show-submenu">Pemerintahan <i class="icon-down-open-mini"></i></a>
                                <ul>
                                    <li><a href="<?php echo site_url();?>">Struktur Organisasi</a></li>
                                    <li><a href="<?php echo site_url();?>">Profil Desa</a></li>
                                </ul>
                            </li>
                            <li class="submenu">
                                <a href="javascript:void(0);" class="show-submenu <?php if($this->session->menu=='pengumuman'||$this->session->menu=='agenda'){echo('current-menu');} ?>">Informasi Publik <i class="icon-down-open-mini"></i></a>
                                <ul>
                                    <li><a class="<?php if($this->session->menu=='pengumuman'){echo('current-menu');} ?> " href="<?php echo site_url();?>pengumuman">Pengumuman</a></li>
                                    <li><a class="<?php if($this->session->menu=='agenda'){echo('current-menu');} ?> " href="<?php echo site_url();?>agenda">Agenda</a></li>
                                    <li><a href="<?php echo site_url();?>">Berita</a></li>
                                    <li><a href="<?php echo site_url();?>">Dana Desa</a></li>
                                    <li><a href="<?php echo site_url();?>">Data Desa</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo site_url();?>galeri">Galeri</a></li>
                            <li><a href="<?php echo site_url();?>">Kontak Kami</a></li>
                            <li><a class="<?php if($this->session->menu!='covid-19'){echo('covid-menu');} else {echo ('current-menu');} ?>" href="<?php echo site_url();?>covid-19">COVID - 19</a></li>
                        </ul>
                    </div>
                    <ul id="top_tools">
                        <li>
                            <a href="javascript:void(0);" class="search-overlay-menu-btn"><i class="icon_search"></i></a>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <?php if (($this->session->menu!='home')== 'active') { ?>
        <section class="parallax-window" data-parallax="scroll" data-image-src="<?php echo base_url();?>assets/img/home_1.jpg" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1><?php echo $page->title;?></h1>
				<p><?php echo $page->title_sub;?></p>
			</div>
		</div>
	</section>

    <?php } ?>
    <main>
    <?php echo $content ?>
    <?php echo $footer ?>