<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_informasi_publik');
		$this->load->library('main');
		$this->main->check_admin();
	}

	public function index()
	{
		$data = $this->main->data_main();
		$where = array(
				'status' => 'agenda'
			);
		$agendas = $this->m_informasi_publik->get_data($where)->result();
        foreach ($agendas as $key) {
            $year =  substr ($key->date,0,4);
            $month =  substr ($key->date,5,2);
            $date =  substr ($key->date,8,2);
            $date = $date."-".$month."-".$year;
            $key->date = $date;
        }
        $data['agendas'] = $agendas;

		$this->template->set('slider', 'kt-menu__item--active');
		$this->template->set('breadcrumb', 'management Agenda');
		$this->template->load_admin('agenda/index', $data);
	}

	public function createprocess()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_error_delimiters('', '');
		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
				    'date' => form_error('date'),
					'title' => form_error('title'),
					'description' =>form_error('description'),
				)
			));
		} else {
			$data = $this->input->post(NULL);

			$year =  substr ($this->input->post('date'),6,4);
            $month =  substr ($this->input->post('date'),3,2);
            $date =  substr ($this->input->post('date'),0,2);
            $date = $year."-".$month."-".$date;

            $data['date'] = $date;
            $data['status'] = 'agenda';
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['update_at'] = date('Y-m-d H:i:s');

			$this->m_informasi_publik->input_data($data);

			echo json_encode(array(
				'status' => 'success',
				'message' => 'data berhasil diinput',
			));
		}
	}

	public function delete($id)
	{
		$where = array('id' => $id);
		$this->m_informasi_publik->delete_data($where);
	}

	public function update()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'title' => form_error('title'),
					'description' => form_error('description')
                )
			));
		} else {
			$id = $this->input->post('id');
			$data = $this->input->post(NULL);
			$where = array(
				'id' => $id
			);

            $data['update_at'] = date('Y-m-d H:i:s');

			$this->m_informasi_publik->update_data($where, $data);
			echo json_encode(array(
				'status' => 'success',
				'message' => 'data berhasil diinput'
			));
		}
	}
}
