        <div id="position">
			<div class="container">
				<ul>
                    <li><a href="<?php echo site_url();?>">Beranda</a></li>
			        <li><?php echo $page->title;?></li>
				</ul>
				</ul>
			</div>
		</div>

		<div class="container margin_60">
			<div class="main_title">
				<h2>Waspada <span><?php echo $page->title;?></span></h2>
			</div>
			<hr>
			<div class="row magnific-gallery add_bottom_60 ">
				<div class="col-sm-4">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/uALxizffjyc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<div class="col-sm-4">
					<div class="img_wrapper_gallery">
						<div class="img_container_gallery">
							<a href="<?php echo base_url();?>assets/img/promkes-6-Langkah-CTPS.jpg" title="6 Langkah Mencuci Tangan" data-effect="mfp-zoom-in"><img src="<?php echo base_url();?>assets/img/promkes-6-Langkah-CTPS.jpg" alt="6 Langkah Mencuci Tangan" class="img-fluid">
								<i class="icon-resize-full-2"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="img_wrapper_gallery">
						<div class="img_container_gallery">
							<a href="<?php echo base_url();?>assets/img/cara_pakai_masker.jpg" title="Cara Menggunakan Masker" data-effect="mfp-zoom-in"><img src="<?php echo base_url();?>assets/img/cara_pakai_masker.jpg" alt="Cara Menggunakan Masker" class="img-fluid">
								<i class="icon-resize-full-2"></i>
							</a>
						</div>
					</div>
				</div>
			</div>


		</div>