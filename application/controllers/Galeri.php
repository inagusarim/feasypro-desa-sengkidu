<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->library('main');

	}

	public function index()
	{
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'galeri');

        $data = $this->main->data_front();
        $data['visitor'] = $this->main->visitor();
        $data['galeris'] = $this->db->where(array('use' => 'yes'))->get('gallery')->result();

        $this->template->front('galeri', $data);

	}



}
