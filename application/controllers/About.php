<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->library('main');

	}

	public function index()
	{
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'about');

        $data = $this->main->data_front();

        $data['page'] = $this->db->where(array('type' => 'about'))->get('pages')->row();

        $this->template->front('about', $data);

	}



}
