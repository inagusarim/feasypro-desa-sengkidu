        <div id="position">
			<div class="container">
				<ul>
                    <li><a href="<?php echo site_url();?>">Beranda</a></li>
			        <li><a href="#">Informasi Publik</a></li>
                    <li><?php echo $page->title;?></li>
				</ul>
				</ul>
			</div>
		</div>

		<div class="container margin_60">
			<div class="row">
				<div class="col-lg-12 add_bottom_15">
                    <?php foreach($agendas as $datas) { ?>
                        <div class="form_title" style="margin-bottom: 0">
                            <h3>
                                <strong><i class="icon-tags"></i></strong>
                                <span>
                                    <?php
                                        $y =  substr ($datas->date,0,4);
                                        $m =  substr ($datas->date,5,2);
                                        $d =  substr ($datas->date,8,2);
                                        $monnth = $this->main->get_bulan($m);
                                        $date = $d." ".$monnth." ".$y;
                                        echo $date;
                                     ?> /
                                </span>
                                / <?php echo $datas->title ?>
                            </h3>
                        </div>
                        <div class="step">
                            <p>
                                <?php echo $datas->description ?>
                            </p>
                        </div>
					<?php } ?>
                </div>
				<hr>
                <div class="col-lg-12">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
		</div>