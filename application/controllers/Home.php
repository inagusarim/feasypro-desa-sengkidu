<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->library('main');

	}
    
	public function index()
	{
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'home');

        $data = $this->main->data_front();
        $data['visitor'] = $this->main->visitor();
        $data['page'] = $this->db->where(array('type' => 'home'))->get('pages')->row();
        $data['sliders'] = $this->db->where(array('use' => 'yes','status !=' => 'banner' ))->get('slider')->result();
        $data['banner'] = $this->db->where(array('status' => 'banner'))->get('slider')->row();
        $data['pengumumans'] = $this->db
            ->where(array('status' => 'pengumuman'))
            ->order_by('id','DESC')
            ->limit(2, 0)
            ->get('informasi_publik')
            ->result();
        $data['agendas'] = $this->db
            ->where(array('status' => 'agenda'))
            ->order_by('date','DESC')
            ->limit(3, 0)
            ->get('informasi_publik')
            ->result();
        $data['galeris'] = $this->db
            ->order_by('rand()')
            ->limit(5, 0)
            ->get('gallery')
            ->result();

        $this->template->front('home', $data);
		
	}

	

}
