<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Covid_19 extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->library('main');

	}

	public function index()
	{
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'covid-19');

        $data = $this->main->data_front();
        $data['visitor'] = $this->main->visitor();
        $data['page'] = $this->db->where(array('type' => 'covid-19'))->get('pages')->row();

        $this->template->front('covid_19', $data);

	}



}
