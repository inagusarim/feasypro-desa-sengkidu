        <div id="position">
			<div class="container">
				<ul>
                    <li><a href="<?php echo site_url();?>">Beranda</a></li>
			        <li><a href="#">Informasi Publik</a></li>
                    <li><?php echo $page->title;?></li>
				</ul>
				</ul>
			</div>
		</div>

        <div class="container margin_60">
			<div class="row">
				<div class="col-lg-12 add_bottom_15">
                    <?php foreach($pengumumans as $datas) { ?>
                        <div class="form_title" style="margin-bottom: 0">
                            <h3><strong><i class="pe-7s-speaker"></i></strong><span><?php echo $datas->title ?></span></h3>
                        </div>
                        <div class="step">
                            <p>
                                <?php echo $datas->description ?>
                            </p>
                        </div>
                    <?php } ?>
				</div>
                <hr>
                <div class="col-lg-12">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
		</div>
