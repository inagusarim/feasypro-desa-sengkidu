<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita_kategori extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_berita_kategori');
		$this->load->library('main');
		$this->main->check_admin();
	}

	public function index()
	{
		$data = $this->main->data_main();
		$data['berita_kategoris'] = $this->m_berita_kategori->get_data()->result();

		$this->template->set('slider', 'kt-menu__item--active');
		$this->template->set('breadcrumb', 'management Kategori Berita');
		$this->template->load_admin('berita_kategori/index', $data);
	}

	public function createprocess()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_error_delimiters('', '');
		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'title' => form_error('title'),
					'description' => form_error('description'),
				)
			));
		} else {
			$data = $this->input->post(NULL);

			$this->m_berita_kategori->input_data($data);

			echo json_encode(array(
				'status' => 'success',
				'message' => 'data berhasil diinput',
			));
		}
	}

	public function delete($id)
	{
		$where = array('id' => $id);
		$this->m_berita_kategori->delete_data($where);
	}

	public function update()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'title' => form_error('title'),
					'description' => form_error('description')
                )
			));
		} else {
			$id = $this->input->post('id');
			$data = $this->input->post(NULL);
			$where = array(
				'id' => $id
			);

            $this->m_berita_kategori->update_data($where, $data);
			echo json_encode(array(
				'status' => 'success',
				'message' => 'data berhasil diinput'
			));
		}
	}
}
