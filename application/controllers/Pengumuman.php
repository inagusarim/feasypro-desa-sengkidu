<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->library('main');

	}

	public function index()
	{
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'pengumuman');

        $data = $this->main->data_front();
        $data['visitor'] = $this->main->visitor();
        $data['page'] = $this->db->where(array('type' => 'pengumuman'))->get('pages')->row();

        $offset = $this->uri->segment(2);

        $jumlah_data = $this->db->where(array('status' => 'pengumuman'))->order_by('id','DESC')->get('informasi_publik')->num_rows();
        $this->load->library('pagination');
        $config['base_url'] = site_url('pengumuman/');
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 10;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<nav aria-label="Page navigation"><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['num_tag_open'] = '<li class="page-item page-link">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link"><a>';
        $config['cur_tag_close'] = '</a><span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item page-link">';
        $config['next_tagl_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-item page-link" >';
        $config['prev_tagl_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item page-link">';
        $config['last_tagl_close'] = '</li>';

        $this->pagination->initialize($config);

        $data['pengumumans'] = $this->db
            ->where(array('status' => 'pengumuman'))
            ->order_by('id','DESC')
            ->get('informasi_publik', 10, $offset)
            ->result();


        $this->template->front('pengumuman', $data);

	}



}
