<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'home';

$route['galeri'] = 'galeri';
$route['pengumuman'] = 'pengumuman';
$route['pengumuman/(:num)'] = 'pengumuman';
$route['agenda'] = 'agenda';
$route['agenda/(:num)'] = 'agenda';
$route['covid-19'] = 'covid_19';
$route['covid-19/(:num)'] = 'covid_19';

$route['feasypro'] = 'feasypro/login';
$route['feasypro/login'] = 'feasypro/login/process';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
