        <div id="position">
			<div class="container">
				<ul>
					<li><a href="<?php echo site_url();?>">Beranda</a>
					</li>
					</li>
					<li>Profil Desa</li>
				</ul>
			</div>
		</div>

		<div class="container margin_30">
            <div class="main_title">
                <h2><span><?php echo $page->title;?></span></h2>
				<p>
					<?php echo $page->title_sub;?>
				</p>
			</div>
			<hr>
			<div class="row ">
                <?php echo $page->description;?>
			</div>

		</div>
